"""
Advent of Code 2023 : Day 5
===========================
Seyni DIOP : https://gitlab.com/seyni_diop/advent-of-code
"""

import numpy as np

def init_source_to_target(filepath:str, map_str:str) -> list:
    """_summary_

    Args:
        filepath (str): _description_
        map_str (str): _description_

    Returns:
        list: _description_
    """
    r = - 1
    result = []
    to_use_sort = []
    with open(filepath, mode="r", encoding="utf-8") as f:
        while True:
            line = f.readline()
            if not line or (line=='\n' and r == 0):
                break
            if line.replace('\n', '') == map_str:
                r = 0
                continue
            if r == 0:
                nums = [int(n) for n in line.split()]
                to_use_sort.append(nums[1])
                result.append(nums)
    # IMPORTANT : sort nums
    np.argsort(to_use_sort)
    return [result[i] for i in np.argsort(to_use_sort)]

class Mapper():
    """Global class
    """
    def __init__(self, filepath, part) -> None:
        self.filepath = filepath
        self.part = part
        self.get_initial_seeds()
        self.seed_to_soil = init_source_to_target(self.filepath,"seed-to-soil map:")
        self.soil_to_fertilizer = init_source_to_target(self.filepath,"soil-to-fertilizer map:")
        self.fertilizer_to_water = init_source_to_target(self.filepath,"fertilizer-to-water map:")
        self.water_to_light = init_source_to_target(self.filepath, "water-to-light map:")
        self.light_to_temperature = init_source_to_target(self.filepath, "light-to-temperature map:")
        self.temperature_to_humidity = init_source_to_target(self.filepath, "temperature-to-humidity map:")
        self.humidity_to_location = init_source_to_target(self.filepath, "humidity-to-location map:")
        self.matching = {'seed':'soil',
                         'soil':'fertilizer',
                         'fertilizer':'water',
                         'water':'light',
                         'light':'temperature',
                         'temperature':'humidity',
                         'humidity': 'location'
                         }
    def get_initial_seeds(self):
        """Get all ranges of initial seeds by part method
        """
        with open(self.filepath, mode="r", encoding="utf-8") as f:
            line_0 = f.readline().strip().replace('\n', '')[7:] # read _first line
           
            if self.part == 1:
                # read _first line
                self.initial_seeds = [(int(num), int(num)+1) for num in line_0.split()]
            else:
                self.initial_seeds = []
                line_splitted = line_0.split()
                for i, num in enumerate(line_splitted):
                    if i%2==0:
                        self.initial_seeds.append((int(num), int(num)+int(line_splitted[i+1])-1))

    @staticmethod
    def _getter(mapper:list, inputs:list) -> list:
        """_summary_

        Args:
            mapper (list): list of mapper
            inputs (list): _description_

        Returns:
            list: _description_
        """
        all_ranges = []
        for start, end in inputs:
            res = []
            cursor = start
            end_cursor = end
            for num in mapper:
                if num[1] <= cursor < num[1] + num[2]:
                    end_cursor = min(end, num[1] + num[2])
                    res.append((num[0] + (cursor - num[1]), num[0] + (end_cursor - num[1])))
                    cursor = end_cursor
                    if end_cursor == end:
                        break
            if cursor == start or end_cursor != end:
                res.append((cursor, end))

            all_ranges.extend(res)
        return all_ranges

    def get(self, inputs, target_name):
        """For a list of tuple (start, end), return the next (corresponding) list of ranges

        Args:
            inputs (_type_): _description_
            target_name (_type_): _description_

        Raises:
            ValueError: _description_

        Returns:
            _type_: _description_
        """
        if target_name == 'soil':
            mapper = self.seed_to_soil
        elif target_name == 'fertilizer':
            mapper = self.soil_to_fertilizer
        elif target_name == 'water':
            mapper = self.fertilizer_to_water
        elif target_name == 'light':
            mapper = self.water_to_light
        elif target_name == 'temperature':
            mapper = self.light_to_temperature
        elif target_name == 'humidity':
            mapper = self.temperature_to_humidity
        elif target_name == 'location':
            mapper = self.humidity_to_location
        else:
            raise ValueError
        return self._getter(mapper, inputs)

    def transform(self, inputs:list, origin:str, to:str) -> list:
        """_summary_

        Args:
            inputs (list): list of tuple where first element if the range start and the second the end of range
            origin (str): input type (seed/soil/fertilizer/water/light/temperature/humidity/location)
            to (str): output type resulting

        Returns:
            list: list of all range of ouput type
        """
        res = inputs
        k = origin
        while True:
            res = self.get(res, self.matching[k])
            if self.matching[k]!=to:
                k = self.matching[k]
            else:
                break
        return res

def solver(filepath:str, part) -> int:
    """Solver

    Args:
        filepath (str): path of text file
        part (int): game part

    Returns:
        int: the minimum of location according the initial seed
    """
    my_mapper = Mapper(filepath=filepath, part=part)
    locations = my_mapper.transform(my_mapper.initial_seeds,  'seed', 'location')
    flat_locations = [item for sublist in locations for item in sublist]

    return min(flat_locations)

assert solver('test.txt', 1) == 35, "Part 1 failed"
assert solver('test.txt', 2) == 46, "Part 2 failed"

if __name__ == "__main__":
    print(f"Solution part 1: {solver('input.txt', 1)}")
    print(f"Solution part 2: {solver('input.txt', 2)}")
